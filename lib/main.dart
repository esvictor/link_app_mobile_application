import 'package:flutter/material.dart';
import 'package:link_app/routes.dart';
import 'constants.dart';
import 'package:link_app/screens/pre_splash/pre_splash_screen.dart';
import 'package:link_app/theme.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appTittle,
      theme: theme(),
      //CALLING THE SPLAAH SCREEN
      initialRoute: PreSplashScreen.routeName,
      // ROUTES FOR THE ROUTES USED IN THE APP
      routes: routes,
    );
  }
}
