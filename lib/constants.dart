import 'package:flutter/material.dart';
import 'package:link_app/size_config.dart';

// APPLICATION TITTLE
const String appTittle = 'LinkApp';

// COLORS
const kPrimaryColor = Color(0xff00618a);
const kPrimaryLightColor = Color(0xFFFFECDF);
const kSecondaryColor = Color(0xffff0000);
const kTextColor = Color(0xff121212);
const kColorMyGrey = Color(0xff101010);
const kPrimaryColorDark = Color(0xff00112a);

// ANIMATION DURATION
const kAnimationDuration = Duration(milliseconds: 200);
const defaultDuration = Duration(milliseconds: 250);

// DEFAULTS PADDING
const defaultPadding = 16.0;
// TEXT STYLES
final tabsTextStyle = TextStyle(
  fontWeight: FontWeight.w500,
  letterSpacing: 0,
);
final headingStyle = TextStyle(
  fontSize: 28,
  fontWeight: FontWeight.w500,
  color: Color(0xff10101a), // sign in and sign up heading color
);
final smallTextDescriptions = TextStyle(
  fontSize: 12,
  fontWeight: FontWeight.w300,
  color: Color(0xff202020), //
);
final labelTextDescriptions = TextStyle(
  fontSize: 12,
  fontWeight: FontWeight.w600,
  color: Color(0xff202050), //
);
final priceTextDescriptions = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.w600,
  color: Color(0xff202050), //
);
final smallTextDescriptionsTwo = TextStyle(
  fontSize: 12,
  color: Color(0xff808080),
);
final smallTextDescriptionsWhite = TextStyle(
  fontSize: 12,
  color: Color(0xffffffff),
);
final linkStyle = TextStyle(
  color: Colors.blue,
);

// BUTTON STYLES
final continueButton = ButtonStyle(
    backgroundColor: MaterialStateProperty.all<Color>(kPrimaryColorDark),
    elevation: MaterialStateProperty.all(0));
final cancelButton = ButtonStyle(
    backgroundColor:
        MaterialStateProperty.all<Color>(Colors.black.withOpacity(.3)),
    elevation: MaterialStateProperty.all(0));
final exitButton = ButtonStyle(
    backgroundColor:
        MaterialStateProperty.all<Color>(Colors.red.withOpacity(1)),
    elevation: MaterialStateProperty.all(0));

// Form Error
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
const String kEmailNullError = "Please Enter your email";
const String kInvalidEmailError = "Please Enter Valid Email";
const String kPassNullError = "Please Enter your password";
const String kShortPassError = "Password is too short";
const String kMatchPassError = "Passwords don't match";
const String kNamefNullError = "Please Enter your first name";
const String kNamelNullError = "Please Enter your last name";
const String kPhoneNumberNullError = "Please Enter your phone number";
const String kAddressNullError = "Please Enter your address";

final otpInputDecoration = InputDecoration(
  contentPadding:
      EdgeInsets.symmetric(vertical: getProportionateScreenWidth(15)),
  border: outlineInputBorder(),
  focusedBorder: outlineInputBorder(),
  enabledBorder: outlineInputBorder(),
);

OutlineInputBorder outlineInputBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(getProportionateScreenWidth(15)),
    borderSide: BorderSide(color: kTextColor),
  );
}
