import 'package:flutter/widgets.dart';
import 'package:link_app/screens/complete_profile/complete_profile_screen.dart';
import 'package:link_app/screens/forgot_password/forgot_password_screen.dart';
import 'package:link_app/screens/home/components/homeWidgets/components/complete_request.dart';import 'package:link_app/screens/home/components/homeWidgets/components/notifications.dart';
import 'package:link_app/screens/home/home_screen.dart';
import 'package:link_app/screens/otp/otp_screen.dart';
import 'package:link_app/screens/play_screen_and_result/play_screen/one_vs_one.dart';
import 'package:link_app/screens/play_screen_and_result/play_screen/play_screen_group_no_elimination.dart';
import 'package:link_app/screens/pre_splash/pre_splash_screen.dart';
import 'package:link_app/screens/sign_in/sign_in_screen.dart';
import 'package:link_app/screens/splash-welcome/splash_screen.dart';
import 'package:link_app/screens/terms_and_conditions/terms_and_conditions.dart';
import 'screens/sign_up/sign_up_screen.dart';

// We use name route
// All our routes will be available here
final Map<String, WidgetBuilder> routes = {
  PreSplashScreen.routeName: (context) => PreSplashScreen(),
  SplashScreen.routeName: (context) => SplashScreen(),
  SignInScreen.routeName: (context) => SignInScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  SignUpScreen.routeName: (context) => SignUpScreen(),
  CompleteProfileScreen.routeName: (context) => CompleteProfileScreen(),
  OtpScreen.routeName: (context) => OtpScreen(),
  HomeScreen.routeName: (context) => HomeScreen(),
  GroupWithoutElimination.routeName: (context) => GroupWithoutElimination(),
  OneVsOne.routeName: (context) => OneVsOne(),
  TermsAndConditions.routeName: (context) => TermsAndConditions(),
  Notifications.routeName: (context) => Notifications(),
  CompleteRequest.routeName: (context) => CompleteRequest(),
};
