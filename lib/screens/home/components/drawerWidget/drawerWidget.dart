import 'package:flutter/material.dart';
import 'components/depositWithdrawSummary.dart';
import 'components/logout.dart';
import 'components/others.dart';
import 'components/profileSummary.dart';
import 'components/settingsSummary.dart';

class DrawerWidget extends StatefulWidget {
  DrawerWidget({Key key}) : super(key: key);

  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      // Important: Remove any padding from the ListView.
      mainAxisSize: MainAxisSize.max,
      children: [
        DrawerHeader(
          child: ProfileSummary(),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment(
                  0.5, 0.1), // 10% of the width, so there are ten blinds.
              colors: [
                const Color(0xff003139),
                const Color(0xff00618a),
              ], // red to yellow
              tileMode: TileMode.mirror, // repeats the gradient over the canvas
            ),
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(color: Color(0xffdadada), width: 0.5)),
          ),
          child: DepositWidget(),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(color: Color(0xffdadada), width: 0.5)),
          ),
          child: SettingsSummary(),
        ),
        Container(
          child: OthersSummary(),
        ),
        Expanded(
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Color(0xffdadada), width: 0.5)),
              ),
              child: LogoutWidget(),
            ),
          ),
        ),
      ],
    );
  }
}
