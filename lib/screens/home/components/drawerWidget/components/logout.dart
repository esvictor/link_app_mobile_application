import 'package:flutter/material.dart';
import 'package:link_app/constants.dart';
class LogoutWidget extends StatefulWidget {
  LogoutWidget({Key key}) : super(key: key);

  @override
  _LogoutWidgetState createState() => _LogoutWidgetState();
}

class _LogoutWidgetState extends State<LogoutWidget> {
  static const textSettingStyle = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.normal,
  );

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        return showDialog(
          context: context,
          builder: (BuildContext context) {
            ///////////////////////////////////////////////////////////////////////////////////////////
            //                         DIALOG CONFIRM LOG OUT
            ///////////////////////////////////////////////////////////////////////////////////////////
            return buildAlertDialog(context);
          },
        );
      },
      title: Text(
        'LOG OUT',
        style: textSettingStyle,
      ),
      trailing: Container(
        padding: EdgeInsets.only(right: 10),
        child: Icon(
          Icons.exit_to_app,
        ),
      ),
    );
  }
  AlertDialog buildAlertDialog(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(0.0)),
      ),
      title: Text(
        'Log out',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 18),
      ),
      // content: Text('Challenges list here',
      //     textAlign: TextAlign.center),
      // actionsOverflowButtonSpacing: 90,
      content: Container(
        child: Text('Are you sure you wan\'t to log out?'),
      ),
      actions: [
        buildActionOnDialog(context),
      ],
    );
  }
  Padding buildActionOnDialog(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 100,
              child: ElevatedButton(
                style: cancelButton,
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'NO',
                ),
              ),
            ),
            Container(
              width: 100,
              child: ElevatedButton(
                style: continueButton,
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('YES'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
