import 'package:flutter/material.dart';

class Toggle extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<Toggle> {
  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    return Switch(
      value: isSwitched,
      onChanged: (value) {
        setState(() {
          isSwitched = value;
          print(isSwitched);
        });
      },
      activeTrackColor: Color(0xff0091ba),
      activeColor: Color(0xff00618a),
    );
  }
}
