import 'package:flutter/material.dart'; 

class OthersSummary extends StatefulWidget {
  OthersSummary({Key key}) : super(key: key);

  @override
  _OthersSummaryState createState() => _OthersSummaryState();
}

class _OthersSummaryState extends State<OthersSummary> {
  static const textSettingStyle =
      TextStyle(fontSize: 16, fontWeight: FontWeight.normal);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: () {},
          child: ListTile(
            //  leading: Icon(Icons.settings),
            title: Text(
              'Settings and Privacy',
              style: textSettingStyle,
            ),
            // trailing: Container(
            //   padding: EdgeInsets.only(right: 10),
            //   child: Icon(
            //     Icons.arrow_forward_ios,
            //   ),
            // ),
          ),
        ),
        InkWell(
          onTap: () {},
          child: ListTile(
            // leading: Icon(Icons.info),
            title: Text(
              'About ',
              style: textSettingStyle,
            ),
            // trailing: Container(
            //   padding: EdgeInsets.only(right: 10),
            //   child: Icon(
            //     Icons.arrow_forward_ios,
            //   ),
            // ),
          ),
        ),
        InkWell(
          onTap: () {},
          child: ListTile(
            //  leading: Icon(Icons.new_releases_rounded),
            title: Text(
              "What's New",
              style: textSettingStyle,
            ),
            // trailing: Container(
            //   padding: EdgeInsets.only(right: 10),
            //   child: Icon(
            //     Icons.arrow_forward_ios,
            //   ),
            // ),
          ),
        ),
        InkWell(
          onTap: () {},
          child: ListTile(
            //  leading: Icon(Icons.help),
            title: Text(
              "Help Center",
              style: textSettingStyle,
            ),
            // trailing: Container(
            //   padding: EdgeInsets.only(right: 10),
            //   child: Icon(
            //     Icons.arrow_forward_ios,
            //   ),
            // ),
          ),
        ),
      ],
    );
  }
}
