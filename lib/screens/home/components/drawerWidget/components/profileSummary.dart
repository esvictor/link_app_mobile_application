import 'package:flutter/material.dart';
import 'package:link_app/screens/home/components/profileWidgets/profileWidget.dart';

import '../../../sampleData.dart';

class ProfileSummary extends StatefulWidget {
  ProfileSummary({Key key}) : super(key: key);

  @override
  _ProfileSummaryState createState() => _ProfileSummaryState();
}

class _ProfileSummaryState extends State<ProfileSummary> {
  static const textStyle = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.normal,
    color: Colors.white,
  );
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(right: 10),
                width: 70,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: Image(
                    image: AssetImage(
                        SampleData.personalDetails['person']['profileImage']),
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('ID:', style: textStyle),
                        Text(
                          (SampleData.personalDetails['person']['ID'])
                              .toString(),
                          style: textStyle,
                        ),
                      ],
                    ),
                    SizedBox(height: 5),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Username:',
                          style: textStyle,
                        ),
                        Text(
                          (SampleData.personalDetails['person']['userName'])
                              .toString(),
                          style: textStyle,
                        ),
                      ],
                    ),
                    SizedBox(height: 5),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Balance:',
                          style: textStyle,
                        ),
                        Text(
                          (SampleData.personalDetails['person']['balance'])
                                  .toString() +
                              ' ' +
                              SampleData.personalDetails['person']['currency'],
                          style: textStyle,
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              OutlineButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ProfileWidgets(),
                    ),
                  );
                },
                // shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                child: Text(
                  'View Profile',
                  style: TextStyle(
                      fontSize: 12,
                      color: Colors.white,
                      fontWeight: FontWeight.normal),
                ),
                borderSide: new BorderSide(color: Colors.white, width: 0.5),
                shape: StadiumBorder(),
              ),
            ],
          )
        ],
      ),
    );
  }
}
