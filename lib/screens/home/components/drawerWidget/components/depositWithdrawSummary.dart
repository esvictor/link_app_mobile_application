import 'package:flutter/material.dart';

class DepositWidget extends StatefulWidget {
  DepositWidget({Key key}) : super(key: key);

  @override
  _DepositWidgetState createState() => _DepositWidgetState();
}

class _DepositWidgetState extends State<DepositWidget> {
  static const textSettingStyle = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.normal,
  );
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListTile(
          onTap: () {},
          title: Text(
            'Deposit',
            style: textSettingStyle,
          ),
          trailing: Container(
            padding: EdgeInsets.only(right: 10),
            child: Icon(
              Icons.add_box_rounded,
            ),
          ),
        ),
        ListTile(
          onTap: () {},
          title: Text(
            'Withdraw',
            style: textSettingStyle,
          ),
          trailing: Container(
            padding: EdgeInsets.only(right: 10),
            child: Icon(Icons.outbox),
          ),
        ),
      ],
    );
  }
}
