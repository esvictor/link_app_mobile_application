import 'package:flutter/material.dart';
import 'package:link_app/screens/home/components/drawerWidget/components/settingsToggle.dart';

class SettingsSummary extends StatefulWidget {
  SettingsSummary({Key key}) : super(key: key);

  @override
  _SettingsSummaryState createState() => _SettingsSummaryState();
}

class _SettingsSummaryState extends State<SettingsSummary> {
  static const textSettingStyle = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.normal,
  );
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          title: Text(
            'Sound Effects',
            style: textSettingStyle,
          ),
          dense: true,
          trailing: Toggle(),
        ),
        ListTile(
          title: Text(
            'Vibration',
            style: textSettingStyle,
          ),
          dense: true,
          trailing: Toggle(),
        ),
      ],
    );
  }
}
