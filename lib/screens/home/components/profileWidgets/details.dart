import 'package:flutter/material.dart';
import 'package:link_app/screens/home/components/profileWidgets/components/textData.dart';
import '../../sampleData.dart';
import 'components/details/account.dart';
import 'components/details/profileDetails.dart';
import 'components/records/statistics.dart';

class UserDetails extends StatefulWidget {
  UserDetails({Key key}) : super(key: key);

  @override
  _UserDetailsState createState() => _UserDetailsState();
}

class _UserDetailsState extends State<UserDetails> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            child: ProfileDetails(
              myImage: SampleData.personalDetails['person']['profileImage'],
              fullName: SampleData.personalDetails['person']['userName'],
              email: SampleData.personalDetails['person']['email'],
            ),
          ),
          Card(
            elevation: 3,
            child: Container(
              decoration: const BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    width: 1.0,
                    color: Color(0XFFDDDDDD),
                  ),
                ),
              ),
              padding: EdgeInsets.only(top: 5, bottom: 5),
              width: double.infinity,
              child: Column(
                children: [
                  Text(
                    'User Details',
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        color: Color(0xff00618A),
                        fontWeight: FontWeight.w500,
                        fontSize: 18),
                  ),
                  TextData(
                      text: 'First name',
                      data: SampleData.personalDetails['person']['firstName']),
                  TextData(
                      text: 'Second name',
                      data: SampleData.personalDetails['person']['middleName']),
                  TextData(
                      text: 'Last name',
                      data: SampleData.personalDetails['person']['lastName']),
                  TextData(
                      text: 'Gender',
                      data: SampleData.personalDetails['person']['gender'].toUpperCase()),
                ],
              ),
            ),
          ),
          Account(),
          Statistics(),
        ],
      ),
    );
  }
}
