import 'package:flutter/material.dart';
import 'package:link_app/screens/home/components/profileWidgets/details.dart';
import 'package:link_app/screens/home/components/profileWidgets/records.dart';
import '../../sampleData.dart';

class ProfileWidgets extends StatelessWidget {
  static String routeName = "/profile_widget";

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title:Text( SampleData.personalDetails['person']['userName']),
          elevation: 2,
          bottom: TabBar(
            indicatorColor: Color(0xffffffff),
            isScrollable: false,
            indicatorWeight: 3,
            tabs: <Widget>[
              Container(
                padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: Text(
                  'DETAILS',
                ),
              ),
              Container(
                padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: Text(
                  'ACHIEVEMENTS',
                ),
              ),
            ],
          ),
          centerTitle: false,
          actions: <Widget>[

            PopupMenuButton(
              onSelected: (result) {
                if (result == 1) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ProfileWidgets(),
                    ),
                  );
                }
              },
              icon: Icon(Icons.more_vert),
              itemBuilder: (context) => [
                PopupMenuItem(
                  child: Text("Edit Profile"),
                  value: 1,
                ),
              ],
            ),
          ],
        ),
        body: TabBarView(
          children: <Widget>[
            UserDetails(),
            UserRecords(),
          ],
        ),
      ),
    );
  }
}
