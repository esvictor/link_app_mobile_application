import 'package:flutter/material.dart'; 
import '../../../../sampleData.dart';
import '../textButton.dart';
import '../textData.dart';

class Account extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      child: Container(
        decoration: const BoxDecoration(
            border: Border(
                bottom: BorderSide(width: 1.0, color: Color(0XFFDDDDDD),),),),
        padding: EdgeInsets.only(top: 10, bottom: 10),
        width: double.infinity,
        child: Column(
          children: [
            Text(
              'Account',
              textAlign: TextAlign.start,
              style: TextStyle(
                  color: Color(0xff00618A),
                  fontWeight: FontWeight.w500,
                  fontSize: 18),
            ),
            TextData(text: 'Amount', data:SampleData.personalDetails['person']['balance'].toString()),
            TextData(text: 'Withdrawable', data: (SampleData.personalDetails['person']['balance'] - SampleData.personalDetails['person']['balance']*0.05).toStringAsFixed(2)),
            MyTextButton(
              text: 'Withdraw',
              iconName: Icons.account_balance_wallet_sharp,
            ),
            MyTextButton(
              text: 'Deposit',
              iconName: Icons.add_box_rounded,
            ),
            MyTextButton(
              text: 'Transactions',
              iconName: Icons.article_outlined,
            ),
          ],
        ),
      ),
    );
  }
}
