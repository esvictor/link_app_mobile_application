import 'package:flutter/material.dart';
import 'package:link_app/constants.dart';

class ProfileDetails extends StatelessWidget {
  final String myImage;
  final String fullName;
  final String email;

  ProfileDetails(
      {@required this.myImage, @required this.fullName, @required this.email})
      : assert(myImage != null),
        assert(fullName != null),
        assert(email != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 25, bottom: 10),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Stack(children: [
            Container(
              width: 100,
              height: 100,
              child: Column(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: Image(
                      image: AssetImage(myImage),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              right: 0,
              child: GestureDetector(
                onTap: () {
                  return showDialog(context: context,
                    builder: (BuildContext context) {
                      return buildAlertDialog(context);
                    },
                  );
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [BoxShadow(
                        color: Colors.grey,
                      )
                      ],
                      borderRadius: BorderRadius.all(Radius.circular(100))),
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Icon(
                      Icons.add_a_photo_rounded,
                      size: 18,
                      color: Colors.black54,
                    ),
                  ),
                ),
              ),
            )
          ]),
          SizedBox(height: 10,),
          Container(
            padding: EdgeInsets.only(top: 5),
            child: Text(
              fullName,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color(0XFF3333333),
                  fontSize: 18),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 5),
            child: Text(
              email,
              style: TextStyle(
                  fontWeight: FontWeight.normal, color: Color(0XFF555555)),
            ),
          ),
        ],
      ),
    );
  }
  AlertDialog buildAlertDialog(BuildContext context) {
    return AlertDialog(
      title: Text(
        'Update profile photo',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 18),
      ),
      // content: Text('Challenges list here',
      //     textAlign: TextAlign.center),
      // actionsOverflowButtonSpacing: 90,
      content: Container(
        child: Text('Upload file form here [________]'),
      ),
      actions: [
        buildActionOnDialog(context),
      ],
    );
  }
  Padding buildActionOnDialog(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 100,
              child: ElevatedButton(
                style: cancelButton,
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'CANCEL',
                ),
              ),
            ),
            Container(
              width: 100,
              child: ElevatedButton(
                style: continueButton,
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('UPDATE'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
