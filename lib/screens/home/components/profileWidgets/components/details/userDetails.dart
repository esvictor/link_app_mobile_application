// import 'package:flutter/material.dart';
// import '../textData.dart';
//
// class UserDetails extends StatelessWidget {
//   var String firstName;
//   final String middleName;
//   final String lastName;
//   final String phone;
//   final String gender;
//
//   UserDetails({
//     @required this.firstName,
//     @required this.middleName,
//     @required this.lastName,
//     @required this.phone,
//     @required this.gender,
//   });
//
//   @override
//   Widget build(BuildContext context) {
//     return Card(
//       elevation: 3,
//       child: Container(
//         decoration: const BoxDecoration(
//           border: Border(
//             bottom: BorderSide(
//               width: 1.0,
//               color: Color(0XFFDDDDDD),
//             ),
//           ),
//         ),
//         padding: EdgeInsets.only(top: 10, bottom: 10),
//         width: double.infinity,
//         child: Column(
//           children: [
//             Text(
//               'User Details',
//               textAlign: TextAlign.start,
//               style: TextStyle(
//                   color: Color(0xff00618A),
//                   fontWeight: FontWeight.w500,
//                   fontSize: 18),
//             ),
//             TextData(text: 'First name', data: firstName),
//             TextData(text: 'Middle name', data: middleName),
//             TextData(text: 'Last name', data: lastName),
//             TextData(text: 'Gender', data: gender),
//             TextData(text: 'Phone', data: phone),
//           ],
//         ),
//       ),
//     );
//   }
// }
