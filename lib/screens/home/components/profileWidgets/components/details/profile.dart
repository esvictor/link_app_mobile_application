import 'package:flutter/material.dart'; 
import 'account.dart';
import '../records/statistics.dart';
import 'profileDetails.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState(title);
}

class _MyHomePageState extends State<MyHomePage> {
  final String myAppName;
  _MyHomePageState(this.myAppName);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(),
      appBar: new AppBar(
        backgroundColor: Color(0xff00618A),
        title: new Text(
          myAppName,
          style: TextStyle(fontSize: 16.0),
        ),
        centerTitle: true,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: IconButton(
                tooltip: 'Edit Details',
                icon: Icon(Icons.edit),
                onPressed: () => {print('Edit Details')}),
          ),
        ],
        elevation: 0.0,
      ),

      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.only(left: 0, right: 0),
          child: Column(
            children: <Widget>[
              ProfileDetails(
                  myImage: 'assets/images/profile.jpg',
                  fullName: 'Victor Ephraim',
                  email: 'ephraimvictor21@gmail.com'),
              Account(),
              Statistics(),
            ],
          ),
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
