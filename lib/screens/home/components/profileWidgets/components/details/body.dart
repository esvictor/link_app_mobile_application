// import 'package:flutter/material.dart';
// import 'file:///C:/flutterApplications/link_app_mobile_application/lib/screens/home/components/profileWidgets/components/details/profileDetails.dart';
// import 'file:///C:/flutterApplications/link_app_mobile_application/lib/screens/home/components/profileWidgets/components/records/statistics.dart';
// import 'file:///C:/flutterApplications/link_app_mobile_application/lib/screens/home/components/profileWidgets/components/details/userDetails.dart';
//
// import '../../../../sampleData.dart';
// import 'account.dart';
//
// class UserDetailsBody extends StatefulWidget {
//   UserDetailsBody({Key key, this.title}) : super(key: key);
//   final String title;
//   @override
//   _MyUserDetailsBody createState() => _MyUserDetailsBody();
// }
//
// class _MyUserDetailsBody extends State<UserDetailsBody> with SingleTickerProviderStateMixin {
//   final List<Tab> myTabs = <Tab>[
//     Tab(text: 'PROFILE'),
//     Tab(text: 'ACHIEVEMENTS'),
//   ];
//
//   TabController _tabController;
//
//   @override
//   void initState() {
//     super.initState();
//     _tabController = TabController(length: myTabs.length, vsync: this);
//   }
//
//   @override
//   void dispose() {
//     _tabController.dispose();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         bottom: TabBar(
//           controller: _tabController,
//           indicatorColor: Colors.white,
//           indicatorWeight: 3,
//           tabs: myTabs,
//         ),
//         backgroundColor: Color(0xff00618a),
//         title: Text(widget.title),
//         centerTitle: false,
//         actions: <Widget>[
//           Padding(
//             padding: const EdgeInsets.only(right: 0.0),
//             child: IconButton(
//                 tooltip: 'More Details',
//                 icon: Icon(
//                   Icons.more_vert,
//                   size: 25,
//                 ),
//                 onPressed: () => {print('More Details')}),
//           ),
//         ],
//       ),
//       UserDetailsBody: TabBarView(
//         controller: _tabController,
//         children: myTabs.map((Tab tab) {
//           // final String label = tab.text.toLowerCase();
//           return SingleChildScrollView(
//             child: Column(
//               children: [
//                 Container(
//                   child: ProfileDetails(
//                     myImage: SampleData.personalDetails['person']
//                         ['profileImage'],
//                     fullName: SampleData.personalDetails['person']['userName'],
//                     email: SampleData.personalDetails['person']['email'],
//                   ),
//                 ),
//                 UserDetails(
//                   firstName: SampleData.personalDetails['person']['firstName'],
//                   middleName: SampleData.personalDetails['person']
//                       ['middleName'],
//                   lastName: SampleData.personalDetails['person']['lastName'],
//                   gender: SampleData.personalDetails['person']['gender'],
//                   phone: SampleData.personalDetails['person']['phone'],
//                 ),
//                 Account(),
//                 Statistics(),
//               ],
//             ),
//           );
//         }).toList(),
//       ),
//     );
//   }
// }
