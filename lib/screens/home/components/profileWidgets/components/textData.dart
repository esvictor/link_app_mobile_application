import 'package:flutter/material.dart';

class TextData extends StatelessWidget {
  final String text;
  final String data;
  const TextData({@required this.text, @required this.data})
      : assert(text != null),
        assert(data != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      padding: EdgeInsets.only(left: 15.0, right: 15.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            text,
          ),
          Text(
            data,
          )
        ],
      ),
    );
  }
}
