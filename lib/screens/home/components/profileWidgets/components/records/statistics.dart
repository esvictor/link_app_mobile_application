import 'package:flutter/material.dart';
import '../textData.dart';

class Statistics extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      child: Container(
        decoration: const BoxDecoration(),
        padding: EdgeInsets.only(top: 10, bottom: 10),
        width: double.infinity,
        child: Column(
          children: [
            Text(
              'Statistics',
              textAlign: TextAlign.start,
              style: TextStyle(
                  color: Color(0xff00618A),
                  fontWeight: FontWeight.w500,
                  fontSize: 18),
            ),
            TextData(text: 'Challenges', data: '10'),
            TextData(text: 'Ranking', data: '4/5'),
            TextData(text: 'Win per Challenge', data: '9(10)'),
            TextData(text: 'Win Streak', data: '6'),
          ],
        ),
      ),
    );
  }
}
