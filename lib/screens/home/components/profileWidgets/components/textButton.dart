import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyTextButton extends StatelessWidget {
  final String text;
  final IconData iconName;
  const MyTextButton({@required this.text, @required this.iconName})
      : assert(text != null),
        assert(iconName != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      padding: EdgeInsets.only(left: .0, right: .0),
      margin: EdgeInsets.only(top: 5),
      child: RaisedButton(
        onPressed: () => {print('Clicked')},
        color: Colors.white,
        elevation: 0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              text,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
            ),
            Icon(
              iconName,
              color: Color(0xff555555),
            ),
          ],
        ),
      ),
    );
  }
}
