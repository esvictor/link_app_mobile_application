import 'package:flutter/material.dart';
import 'package:link_app/screens/play_screen_and_result/play_screen/one_vs_one.dart';
import 'package:link_app/screens/play_screen_and_result/play_screen/play_screen_group_no_elimination.dart';

class Requests extends StatefulWidget {
  Requests({Key key}) : super(key: key);

  @override
  _RequestsState createState() => _RequestsState();
}

class _RequestsState extends State<Requests> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          GestureDetector(
            //PROBLEM HERE CALLING PLAY SCREEN
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => OneVsOne()),
              )
            },
            child: Text("In Progress, Click to One v One"),
          ),
          SizedBox(
            height: 20,
          ),
          GestureDetector(
            //PROBLEM HERE CALLING PLAY SCREEN
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => GroupWithoutElimination()),
              )
            },
            child: Text("In Progress, Group no Elimination"),
          ),
        ],
      ),
    );
  }
}
