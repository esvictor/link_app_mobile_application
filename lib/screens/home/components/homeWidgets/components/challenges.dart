import 'package:flutter/material.dart';
import 'package:link_app/screens/home/sampleData.dart';
import '../../../../../constants.dart';

class ScopeAndChallenges extends StatefulWidget {
  ScopeAndChallenges({Key key}) : super(key: key);

  @override
  _ScopeAndChallengesState createState() => _ScopeAndChallengesState();
}

class _ScopeAndChallengesState extends State<ScopeAndChallenges> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: BouncingScrollPhysics(),
      shrinkWrap: false,
      scrollDirection: Axis.vertical,
      itemCount: SampleData.items.length,
      itemBuilder: (BuildContext context, int indexScope) => ExpansionTile(
        initiallyExpanded: false,
        title: Text(
          SampleData.items[indexScope],
          style: TextStyle(
            color: Color(0xff00618a),
            fontSize: 16.0,
            fontWeight: FontWeight.w600,
          ),
        ),
        children: <Widget>[
          ListTile(
            title: Text(
              '${SampleData.items[indexScope]} Scope Descriptions Here',
              style: smallTextDescriptionsTwo,
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 250,
            child: ListView.builder(
                physics: BouncingScrollPhysics(),
                shrinkWrap: false,
                scrollDirection: Axis.horizontal,
                itemCount: SampleData.images.length,
                itemBuilder: (BuildContext context, int indexCategory) {
                  String categoryToCheckIfFavored =
                      SampleData.challenges[indexCategory];
                  var isFavoredCategory = SampleData.favoredCategory
                      .contains(categoryToCheckIfFavored);
                  return GestureDetector(
                    onTap: () {
                      return showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          ///////////////////////////////////////////////////////////////////////////////////////////
                          //                         DIALOG WITH CATEGORY CHALLENGES
                          ///////////////////////////////////////////////////////////////////////////////////////////
                          return buildAlertDialog(indexCategory, context);
                        },
                      );
                    },
                    child: Padding(
                      padding: indexCategory == 0
                          ? EdgeInsets.only(left: 8.0)
                          : EdgeInsets.all(0.0),
                      child: Container(
                        margin:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                        // height: 600,
                        width: MediaQuery.of(context).size.width * 0.4,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            boxShadow: [
                              BoxShadow(
                                color: kPrimaryColor.withAlpha(40),
                                spreadRadius: 1,
                                blurRadius: 5,
                              )
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.baseline,
                                children: [
                                  ////////////////////////////////////////////////////////////////////////////////////////////
                                  //                                       PIN CATEGORY
                                  ///////////////////////////////////////////////////////////////////////////////////////////
                                  Icon(
                                    Icons.push_pin,
                                    color: kPrimaryColor,
                                  ),
                                  ///////////////////////////////////////////////////////////////////////////////////////////
                                  //                                FAVORITE THE WHOLE CATEGORY ICON GESTURE
                                  ///////////////////////////////////////////////////////////////////////////////////////////
                                  GestureDetector(
                                    onTap: () {
                                      ///////////////////////////////////////////////////////////////////////////////////////
                                      //                                  DEBUG PRINT
                                      ///////////////////////////////////////////////////////////////////////////////////////
                                      print(
                                          'favored all the challenges in Scope ${indexScope + 1} - category number ${indexCategory + 1}');
                                      setState(() {
                                        if (isFavoredCategory) {
                                          SampleData.favoredCategory.remove(
                                              SampleData
                                                  .challenges[indexCategory]);
                                        } else {
                                          SampleData.favoredCategory.add(
                                              SampleData
                                                  .challenges[indexCategory]);
                                        }
                                      });
                                    },
                                    child: isFavoredCategory == true
                                        ? Icon(Icons.favorite,
                                            color:
                                                kPrimaryColor.withOpacity(0.8))
                                        : Icon(Icons.favorite_border,
                                            color: Colors.black38),
                                  ),
                                ],
                              ),
                              /////////////////////////////////////////////////////////////////////////////////////////
                              //                               CATEGORY IMAGE
                              /////////////////////////////////////////////////////////////////////////////////////////
                              Expanded(
                                child: Center(
                                  child: Image.asset(
                                      'assets/images/${SampleData.images[indexCategory]}'),
                                ),
                              ),
                              /////////////////////////////////////////////////////////////////////////////////////////
                              //                             CATEGORY DESCRIPTION
                              /////////////////////////////////////////////////////////////////////////////////////////
                              Container(
                                child: Center(
                                  child: Text(
                                    'Category ${indexCategory + 1} of ${SampleData.items[indexScope]}',
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }

  AlertDialog buildAlertDialog(int indexCategory, BuildContext context) {
    return AlertDialog(
      titlePadding: EdgeInsets.all(0),
      buttonPadding: EdgeInsets.all(0),
      actionsPadding: EdgeInsets.all(0),
      contentPadding: EdgeInsets.only(
          left: 0, right: 0, top: defaultPadding, bottom: defaultPadding),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(0.0)),
      ),
      title: Container(
        color: kPrimaryColorDark,
        child: Padding(
          padding: const EdgeInsets.only(
              top: defaultPadding, bottom: defaultPadding),
          child: Text(
            'Category ${indexCategory + 1} - Challenges (${SampleData.challenges.length})',
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
        ),
      ),
      // content: Text('Challenges list here',
      //     textAlign: TextAlign.center),
      // actionsOverflowButtonSpacing: 90,
      content: Container(
        // color: Colors.red,
        height: MediaQuery.of(context).size.height * 0.5,
        child: ListView.builder(
            /////////////////////////////////////////////////////////////////////////////////////////
            //                           GENERATE A DIALOG OF LIST OF CHALLENGES IN A CATEGORY
            /////////////////////////////////////////////////////////////////////////////////////////
            physics: BouncingScrollPhysics(),
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemCount: SampleData.challenges.length,
            itemBuilder: (BuildContext context, int indexChallenges) {
              String challengeToCheckIfFavored =
                  SampleData.challenges[indexChallenges];
              var isFavored = SampleData.favoredChallenges
                  .contains(challengeToCheckIfFavored);
              return StatefulBuilder(
                  builder: (BuildContext context, StateSetter setState) {
                return GestureDetector(
                  onTap: () {
                    /////////////////////////////////////////////////////////////////////////////
                    //                               DEBUG TEXT
                    /////////////////////////////////////////////////////////////////////////////
                    print('favored! ${indexChallenges + 1}');
                    setState(() {
                      ///////////////////////////////////////////////////////////////////////////
                      //                ADD AND REMOVE FAVORITE CHALLENGES OF A CATEGORY
                      ///////////////////////////////////////////////////////////////////////////
                      if (isFavored) {
                        SampleData.favoredChallenges
                            .remove(SampleData.challenges[indexChallenges]);
                        isFavored = false;
                      } else {
                        SampleData.favoredChallenges
                            .add(SampleData.challenges[indexChallenges]);
                        isFavored = true;
                      }
                    });
                  },
                  child: ListTile(
                      title: Text(
                          '${indexChallenges + 1}\). ${SampleData.challenges[indexChallenges]}'),
                      trailing: isFavored == true
                          ? Icon(Icons.star)
                          : Icon(Icons.star_border_outlined)),
                );
              });
            }),
      ),
      actions: [
        Container(
            decoration: BoxDecoration(
                color: Colors.transparent,
                border:
                    Border(top: BorderSide(color: kColorMyGrey, width: 0.5))),
            child: buildActionOnDialog(context)),
      ],
    );
  }

  Padding buildActionOnDialog(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 100,
              child: ElevatedButton(
                style: cancelButton,
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'CANCEL',
                ),
              ),
            ),
            Container(
              width: 100,
              child: ElevatedButton(
                style: continueButton,
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('SAVE'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
