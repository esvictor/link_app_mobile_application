import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:link_app/screens/home/components/homeWidgets/components/complete_request.dart';
import 'package:link_app/screens/home/sampleData.dart';
import '../../../../../constants.dart';

class Favorites extends StatefulWidget {
  final List<String> favoredChallenges;

  Favorites({Key key, @required this.favoredChallenges}) : super(key: key);

  @override
  _FavoritesState createState() => _FavoritesState();
}

class _FavoritesState extends State<Favorites> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: BouncingScrollPhysics(),
      shrinkWrap: false,
      scrollDirection: Axis.vertical,
      itemCount: SampleData.favoredChallenges.length,
      itemBuilder: (BuildContext context, int indexFavored) => ExpansionTile(
        initiallyExpanded: false,
        title: Text(
          SampleData.favoredChallenges[indexFavored],
          style: TextStyle(
            color: Color(0xff00618a),
            fontSize: 16.0,
            fontWeight: FontWeight.w600,
          ),
        ),
        children: <Widget>[
          Container(
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.only(
                  left: defaultPadding,
                  right: defaultPadding,
                  top: 0,
                  bottom: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    ' Minimum cost for this challenge',
                    style: smallTextDescriptions,
                    textAlign: TextAlign.left,
                  ),
                  Stack(
                    // alignment: ,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                            left: defaultPadding / 4,
                            right: defaultPadding / 4),
                        child: Text(
                          '12,500',
                          style: priceTextDescriptions,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        left: 0,
                        right: 0,
                        child: Container(
                          height: 5,
                          color: Colors.deepOrangeAccent.withOpacity(0.3),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Divider(
            height: 0,
          ),
          SizedBox(
            child: ListView.builder(
              itemCount: SampleData.challengeLevel.length,
              itemBuilder: (BuildContext context, int indexChallengeLevel) =>
                  Column(
                children: [
                  Container(
                    child: ListTile(
                      title: Text(
                          // DIALOG BOX TITLE
                          '${(SampleData.challengeLevel[indexChallengeLevel]).toUpperCase()}',
                          style: labelTextDescriptions),
                      trailing: ElevatedButton(
                        style: cancelButton,
                        onPressed: () {
                          return showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return buildAlertDialog(
                                  indexFavored, indexChallengeLevel, context);
                            },
                          );
                        },
                        child: Text(
                          'REQUEST',
                          style: smallTextDescriptionsWhite,
                        ),
                      ),
                    ),
                  ),
                  indexChallengeLevel != SampleData.challengeLevel.length - 1
                      ? Divider(
                          height: 0,
                        )
                      : Container()
                ],
              ),
              shrinkWrap: true,
            ),
          )
        ],
      ),
    );
  }

  AlertDialog buildAlertDialog(
      int indexChallenge, int indexChallengeLevel, BuildContext context) {
    return AlertDialog(
      titlePadding: EdgeInsets.all(0),
      contentPadding: EdgeInsets.all(defaultPadding),
      title: Container(
        color: kPrimaryColorDark,
        child: Padding(
          padding: const EdgeInsets.only(
            top: defaultPadding,
            bottom: defaultPadding,
          ),
          child: Text(
            // DIALOG PLAY MODE TITLE
            '${SampleData.favoredChallenges[indexChallenge].toString().toUpperCase()} - ${SampleData.challengeLevel[indexChallengeLevel].toString().toUpperCase()}',
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 11),
          ),
        ),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            // color: Colors.red,
            child: ListView.builder(
                /////////////////////////////////////////////////////////////////////////////////////////
                //                 GENERATE A DIALOG OF LIST OF CHALLENGES IN A CATEGORY
                /////////////////////////////////////////////////////////////////////////////////////////
                physics: BouncingScrollPhysics(),
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: SampleData.playModes.length,
                itemBuilder: (BuildContext context, int indexPlayModes) {
                  return StatefulBuilder(
                      builder: (BuildContext context, StateSetter setState) {
                    return GestureDetector(
                      onTap: () {
                        print('$indexPlayModes');
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CompleteRequest(
                                    challengePlayModes:
                                        SampleData.playModes[indexPlayModes],
                                    challengeLevel: SampleData
                                        .challengeLevel[indexChallengeLevel],
                                    categoryChallenge: SampleData
                                        .favoredChallenges[indexChallenge],
                                  )),
                        );
                      },
                      child: Column(
                        children: [
                          Container(
                            child: ListTile(
                                title: Text(
                                    '${SampleData.playModes[indexPlayModes]}'),
                                trailing: Icon(Icons.assistant_photo_outlined)),
                          ),
                          indexPlayModes != SampleData.playModes.length - 1
                              ? Divider(
                                  height: 0,
                                )
                              : Container(),
                        ],
                      ),
                    );
                  });
                }),
          ),
        ],
      ),
      buttonPadding: EdgeInsets.all(0),
      actionsPadding: EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(0.0)),
      ),
      actions: [
        Container(child: buildActionOnDialog(context)),
      ],
    );
  }

  Padding buildActionOnDialog(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0.0, right: 0.0),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border(top: BorderSide(color: kColorMyGrey, width: 0.5))),
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 100,
              child: ElevatedButton(
                style: cancelButton,
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'CANCEL',
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
