import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:link_app/screens/home/components/homeWidgets/homeWidget.dart';
import '../../../../../constants.dart';

class CompleteRequest extends StatefulWidget {
  static String routeName = '/completeRequest';
  final String challengePlayModes;
  final String challengeLevel;
  final String categoryChallenge;

  CompleteRequest({
    Key key,
    this.challengePlayModes,
    this.challengeLevel,
    this.categoryChallenge,
  }) : super(key: key);

  @override
  _CompleteRequestState createState() => _CompleteRequestState();
}

class _CompleteRequestState extends State<CompleteRequest> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Complete Request'),
      ),
      body: SafeArea(
        child: CustomScrollView(
          shrinkWrap: true,
          slivers: [
            SliverList(
              delegate: SliverChildListDelegate([
                Container(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 8.0, bottom: 8.0, left: 16, right: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'CHALLENGE:',
                          style: smallTextDescriptionsTwo,
                        ),
                        Text(
                          '${widget.categoryChallenge.toUpperCase()}',
                          style: labelTextDescriptions,
                        ),
                      ],
                    ),
                  ),
                ),
                Divider(
                  height: 0,
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 8.0, bottom: 8.0, left: 16, right: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'LEVEL:',
                          style: smallTextDescriptionsTwo,
                        ),
                        Text(
                          '${widget.challengeLevel.toUpperCase()}',
                          style: labelTextDescriptions,
                        ),
                      ],
                    ),
                  ),
                ),
                Divider(
                  height: 0,
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 8.0, bottom: 8.0, left: 16, right: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'PLAY MODE:',
                          style: smallTextDescriptionsTwo,
                        ),
                        Text(
                          '${widget.challengePlayModes.toUpperCase()}',
                          style: labelTextDescriptions,
                        ),
                      ],
                    ),
                  ),
                ),
                Divider(
                  height: 0,
                ),
              ]),
            ),
            SliverList(
              delegate: SliverChildListDelegate([
                SizedBox(
                  height: 40,
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: defaultPadding * 2,
                        right: defaultPadding * 2,
                        top: defaultPadding,
                        bottom: defaultPadding),
                    child: Column(
                      children: [],
                    ),
                  ),
                ),
              ]),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              fillOverscroll: true,
              child: Container(
                child: Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 36.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        FloatingActionButton.extended(
                          elevation: 0,
                          backgroundColor: Color(0xff00618a),
                          heroTag: 'CONFIRM REQUEST',
                          onPressed: () {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => HomeWidget(title: appTittle,tabNumber: 2,)));
                          },
                          label: Text('CONFIRM REQUEST'),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
