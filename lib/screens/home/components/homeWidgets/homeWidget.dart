import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:link_app/constants.dart';
import 'package:link_app/screens/home/components/drawerWidget/drawerWidget.dart';
import 'package:link_app/screens/home/components/homeWidgets/components/challenges.dart';
import 'package:link_app/screens/home/components/profileWidgets/profileWidget.dart';
import 'components/favorites.dart';
import 'components/notifications.dart';
import 'components/requests.dart';

//HOME SCREEN TABS
class HomeWidget extends StatefulWidget {
  final String title;
  final int tabNumber;
  static String routeName = '/homePage';
  const HomeWidget({Key key, this.title,this.tabNumber}) : super(key: key);
  _HomeWidget createState() => _HomeWidget();
}

class _HomeWidget extends State<HomeWidget>
    with SingleTickerProviderStateMixin {


  @override
  Widget build(BuildContext context) {
   int myTabNumber = widget.tabNumber;
    if(myTabNumber == null){
      myTabNumber = 0;
    }
    //NUMBER DISPLAYED ON NOTIFICATION ICON IF ITS GREATER  THAN 0
    var notificationsCounter = 9;

    return DefaultTabController(
      initialIndex: myTabNumber,
      length: 3,
      child: Scaffold(
        drawer: Drawer(
          //LEFT SIDE DRAWER
          child: DrawerWidget(),
        ),
        appBar: AppBar(
          elevation: 2,
          bottom: TabBar(
            indicatorColor: Color(0xffffffff),
            isScrollable: true,
            indicatorWeight: 3,
            tabs: <Widget>[
              Container(
                padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: Text(
                  'CHALLENGES',
                  style: tabsTextStyle,
                ),
              ),
              Container(
                padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: Text(
                  'FAVORITES',
                  style: tabsTextStyle,
                ),
              ),
              Container(
                padding: const EdgeInsets.only(top: 10.0, bottom: 10),
                child: Text(
                  'REQUESTS',
                  style: tabsTextStyle,
                ),
              ),
            ],
          ),
          backgroundColor: kPrimaryColor,
          title: Text(widget.title),
          centerTitle: false,
          actions: <Widget>[
            //NOTIFICATION BUTTON
            Stack(
              alignment: Alignment.center,
              children: [
                IconButton(
                  tooltip: 'Notifications',
                  icon: Icon(
                    Icons.notifications,
                    size: 25,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, Notifications.routeName);
                  },
                ),
                notificationsCounter > 0
                    ? Positioned(
                        top: 0,
                        bottom: 0,
                        right: 0,
                        // left: 0,
                        child: Center(
                          child: Container(
                            padding: EdgeInsets.all(2),
                            decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.circular(20)),
                            child: Text(
                              '$notificationsCounter',
                              textAlign: TextAlign.center,
                              style:
                                  TextStyle(color: kPrimaryColor, fontSize: 12),
                            ),
                          ),
                        ),
                      )
                    : Container()
              ],
            ),
            PopupMenuButton(
              onSelected: (result) {
                if (result == 1) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ProfileWidgets(),
                    ),
                  );
                }
              },
              //MORE BUTTON
              icon: Icon(Icons.more_vert),
              itemBuilder: (context) => [
                PopupMenuItem(
                  child: Text("View Profile"),
                  value: 1,
                ),
                PopupMenuItem(
                  child: Text("Settings"),
                  value: 2,
                ),
                PopupMenuItem(
                  child: Text("What's New"),
                  value: 3,
                ),
              ],
            ),
          ],
        ),
        body: TabBarView(
          children: <Widget>[
            ScopeAndChallenges(),
            Favorites(),
            Requests(),
          ],
        ),
      ),
    );
  }
}

class CircleTabIndicator extends Decoration {
  final BoxPainter _painter;

  CircleTabIndicator({@required Color color, @required double radius})
      : _painter = _CirclePainter(color, radius);

  @override
  BoxPainter createBoxPainter([onChanged]) => _painter;
}

class _CirclePainter extends BoxPainter {
  final Paint _paint;
  final double radius;

  _CirclePainter(Color color, this.radius)
      : _paint = Paint()
          ..color = color
          ..isAntiAlias = true;

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration cfg) {
    final Offset circleOffset =
        offset + Offset(cfg.size.width / 2, cfg.size.height - radius - 5);
    canvas.drawCircle(circleOffset, radius, _paint);
  }
}
