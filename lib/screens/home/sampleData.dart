class SampleData {
  static Map<String, Map> personalDetails = {
    'person': {
      'ID': 01234567,
      'userName': 'exampleUser',
      'email': 'example@example.example',
      'phone': '0980123456',
      'gender': 'male',
      'firstName': 'Example',
      'middleName': 'User',
      'lastName': 'Name',
      'profileImage': 'assets/images/profile.jpg',
      'balance': 101500.59,
      'currency': 'TZS',
    },
  };

  static Map<String, Object> locationDetails = {
    'location': {
      'country': 'Tanzania',
      'region': 'Dar-es-Salaam',
      'address': 'Ilala',
      'latitude': -6.2198313,
      'longitude': 39.9212313,
    }
  };
  static Map<String, Object> scopeCategoriesChallengeLevel = {
    'Scope': {
      'Education': {
        'Mathematics': [
          {
            'Trigonometry': ['Beginner', 'Intermediate', 'Advance', 'Expert']
          },
          {
            'Algerba': ['Beginner', 'Intermediate', 'Advance', 'Expert']
          },
          {
            'Integral': ['Beginner', 'Intermediate', 'Advance', 'Expert'],
          },
          {
            'Artithmetic': ['Beginner', 'Intermediate', 'Advance', 'Expert']
          }
        ],
        'Chemisty': {},
        'Biology': {},
        'Programming': {},
        'Civics': {},
        'History': {},
        'Geography': {}
      },
      // 'Sports': {
      //   'Football': {},
      //   'Basketball': {},
      //   'Boxing': {},
      //   'Tenis': {},
      //   'Running': {},
      //   'Volleyball': {},
      //   'Cricket': {},
      //   'Hockey': {},
      //   'Wrestling': {}
      // },
      // 'Entertainment': {
      //   'Music': {},
      //   'Movies': {},
      //   'TV Series': {},
      //   'Comedy': {},
      //   'Games': {},
      //   'Literature': {},
      // },
      // 'History': {
      //   'Africa': {},
      //   'Europe': {},
      //   'Asia': {},
      //   'America': {},
      // },
      'Others': {}
    },
  };
  static List<String> items = <String>[
    'Education',
    'Sports',
    'Computer',
    'Politics',
    'Entertainment',
    'Others',
  ];
  static List<String> images = <String>[
    'physics.png',
    'math.png',
    'biology.png',
    'chemistry.png',
    'energy.png',
  ];
  static List<String> challenges = <String>[
    'Trigonometry',
    'Algebra',
    'Calculus',
    'Polynomial',
    'Numbering System',
    'Complex Numbers',
    'Earth as Sphere',
    'Linear Programming',
    'Geometry',
    'Simultaneous Equations',
  ];

  static List<String> favoredChallenges = List<String>();
  static List<String> favoredCategory = List<String>();
  static List<String> selectedPlayMode = List<String>();
  static List<String> challengeLevel = <String>[
    'Beginner',
    'Intermediate',
    'Expert'
  ];
  static List<String> playModes = <String>[
    'One v One',
    'Group (Elimination)',
    'Group (No Elimination)'
  ];
  static List<Object> playModeDescriptions = <Object>[
    [500, 'OneVsOneDescription', 'Other'],
    [2000, 'GroupEliDescription', 'Other'],
    [5000, 'GroupNoEliDescription', 'Other']
  ];

}
