import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class SplashContent extends StatelessWidget {
  const SplashContent({
    Key key,
    this.text,
    this.image,
  }) : super(key: key);
  final String text, image;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Spacer(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(5),
                  child: Hero(
                    tag: 'Icon $text',
                    child: Image.asset(
                      'assets/images/linkAppIcon.png',
                      scale: 10,
                    ),
                  ),
                )
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(5),
                  child: Hero(
                    tag: 'Text $text',
                    child: Image.asset(
                      'assets/images/linkAppTexts.png',
                      scale: 10,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          text,
          textAlign: TextAlign.center,
        ),
        Spacer(flex: 2),
        Image.asset(
          image,
          height: getProportionateScreenHeight(265),
          width: getProportionateScreenWidth(235),
        ),
      ],
    );
  }
}
