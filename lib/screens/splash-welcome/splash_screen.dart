import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:link_app/screens/splash-welcome/components/body.dart';
import 'package:link_app/size_config.dart';

class SplashScreen extends StatelessWidget {

  static String routeName = "/welcome";
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    // You have to call it on your starting screen
    SizeConfig().init(context);
    return Scaffold(
      body: Body(),
    );
  }
}
