import 'package:flutter/material.dart';
import 'package:link_app/constants.dart';
import 'package:link_app/components/no_account_text.dart';
import '../../../size_config.dart';
import 'sign_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.03),
                Container(
                  child: Image.asset(
                    'assets/images/linkAppIcon.png',
                    height: 40,
                  ),
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                Text(
                  "Welcome to LinkApp",
                  style: headingStyle,
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.01),
                Text(
                  "Sign in with your email and password",
                  textAlign: TextAlign.center,
                  style: smallTextDescriptions,
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                Divider(),
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                SignForm(),
                // SizedBox(height: SizeConfig.screenHeight * 0.08),
                SizedBox(height: getProportionateScreenHeight(15)),
                NoAccountText(),
                SizedBox(height: 50),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
