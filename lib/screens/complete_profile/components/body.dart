import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:link_app/constants.dart';
import 'package:link_app/screens/terms_and_conditions/terms_and_conditions.dart';
import 'package:link_app/size_config.dart';

import 'complete_profile_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.03),
                Container(
                  child: Image.asset(
                    'assets/images/linkAppIcon.png',
                    height: 40,
                  ),
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                Text("Complete Profile", style: headingStyle),
                Text(
                  "Complete your details",
                  textAlign: TextAlign.center,
                  style: smallTextDescriptions,
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.06),
                CompleteProfileForm(),
                SizedBox(height: getProportionateScreenHeight(30)),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    style: DefaultTextStyle.of(context).style,
                    children: <TextSpan>[
                      TextSpan(
                        text: 'By continuing you are agreeing to the ',
                      ),
                      TextSpan(
                        text: 'Terms and Conditions',
                        style: linkStyle,
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            print('Terms and Conditions"');
                            Navigator.pushNamed(
                                context, TermsAndConditions.routeName);
                          },
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20,)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
