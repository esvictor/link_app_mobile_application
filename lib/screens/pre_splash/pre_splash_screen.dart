import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'package:link_app/screens/splash-welcome/splash_screen.dart';

class PreSplashScreen extends StatefulWidget {
  static String routeName = "/splash";

  PreSplashScreen({Key key}) : super(key: key);

  @override
  _PreSplashScreenState createState() => _PreSplashScreenState();
}

class _PreSplashScreenState extends State<PreSplashScreen> {
  final splashDelay = 7;

  void initState() {
    super.initState();
    _loadWidget();
  }

  _loadWidget() async {
    var _duration = Duration(seconds: splashDelay);
    return Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.pushReplacement(
      context,
      //CALLING THE WELCOME SCREENS HERE
      MaterialPageRoute(builder: (BuildContext context) => SplashScreen()),
    );
  }

  @override
  Widget build(BuildContext context) {
    //DISABLE AUTO ROTATE
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
      body: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(5),
                      child: Image.asset(
                        'assets/images/linkAppIcon.png',
                        scale: 10,
                      ),
                    )
                  ],
                ),
                Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(5),
                      child: Image.asset(
                        'assets/images/linkAppTexts.png',
                        scale: 10,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    'LinkApp Sample Description',
                    style: TextStyle(fontFamily: 'Gilroy'),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
