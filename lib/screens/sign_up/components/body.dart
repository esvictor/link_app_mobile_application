import 'package:flutter/material.dart';
import 'package:link_app/constants.dart';
import 'package:link_app/size_config.dart';
import 'sign_up_form.dart';

class Body extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.03),
                Container(
                  child: Image.asset(
                    'assets/images/linkAppIcon.png',
                    height: 40,
                  ),
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                // 4%
                Text("Create Account", style: headingStyle),
                Text(
                  "Complete your details and continue",
                  textAlign: TextAlign.center,
                  style: smallTextDescriptions,
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.01),
                Divider(),
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                SignUpForm(),
                SizedBox(height: SizeConfig.screenHeight * 0.08),
                SizedBox(height: getProportionateScreenHeight(20)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
