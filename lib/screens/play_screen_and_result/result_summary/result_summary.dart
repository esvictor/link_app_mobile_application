import 'package:flutter/material.dart';

class Summary extends StatelessWidget {
  final int score;
  static int questionNumber;
  static int finalScore;

  Summary({
    Key key,
    @required this.score,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  decoration: BoxDecoration(),
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Stack(
                      children: [
                        CircleAvatar(
                          radius: 60,
                          backgroundColor: Color(0xffdedede),
                          child: CircleAvatar(
                            radius: 58,
                            backgroundColor: Colors.redAccent,
                            child: CircleAvatar(
                              radius: 60,
                              backgroundImage: AssetImage(
                                  'assets/images/female.png'),
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          right: 0,
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(70)),
                                gradient: LinearGradient(
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                  colors: <Color>[
                                    Colors.red,
                                    Colors.black
                                  ],
                                )),
                            child: Padding(
                              padding: const EdgeInsets.all(3.0),
                              child: Text(
                                '$finalScore',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(),
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Text(
                      'VS',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Color(0xff999999)),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(),
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Stack(
                      children: [
                        CircleAvatar(
                          radius: 60,
                          backgroundColor: Color(0xffdedede),
                          child: CircleAvatar(
                            radius: 58,
                            backgroundColor: Colors.blueAccent,
                            child: CircleAvatar(
                              radius: 60,
                              backgroundImage: AssetImage(
                                  'assets/images/profile.jpg'),
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          right: 0,
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(70)),
                                gradient: LinearGradient(
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                  colors: <Color>[
                                    Colors.blue,
                                    Colors.black
                                  ],
                                )),
                            child: Padding(
                              padding: const EdgeInsets.all(3.0),
                              child: Text(
                                '43',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            new Text(
              "Final Score: $score",
              style: new TextStyle(fontSize: 35.0),
            ),

            new Padding(padding: EdgeInsets.all(30.0)),
            MaterialButton(
              color: Colors.blueGrey,
              onPressed: () {
                questionNumber = 0;
                finalScore = 0;
                Navigator.pop(context);
              },
              child: new Text(

                "Rematch",
                style: new TextStyle(fontSize: 20.0, color: Colors.white),
              ),

            ),
            new MaterialButton(
              color: Colors.blueGrey,
              onPressed: () {
                questionNumber = 0;
                finalScore = 0;
                Navigator.pop(context);
              },
              child: new Text(
                "New Challenge",
                style: new TextStyle(fontSize: 20.0, color: Colors.white),
              ),

            ),
            new MaterialButton(
              color: Colors.grey,
              onPressed: () {
                finalScore = 0;
                questionNumber = 0;
                Navigator.pushReplacementNamed(
                  context,
                  '/home',
                );
              },
              child: new Text(
                "Home",
                style: new TextStyle(fontSize: 20.0, color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
