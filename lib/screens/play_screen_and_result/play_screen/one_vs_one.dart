import 'package:flutter/material.dart';
import 'dart:async';
import 'package:link_app/screens/play_screen_and_result/play_screen/question_and_answer_samples.dart';
import 'package:link_app/screens/play_screen_and_result/result_summary/result_summary.dart';
import 'package:link_app/constants.dart';

class OneVsOne extends StatefulWidget {
  static String routeName = "/OneVsOne";

  OneVsOne({Key key}) : super(key: key);

  @override
  _OneVsOneState createState() => _OneVsOneState();
}

var finalScore = 0;
var questionNumber = 0;
var quiz = new QuestionAnswers();

class _OneVsOneState extends State<OneVsOne> {
  double timer = 30;
  String showTimer;
  double totalTime = 30;
  String withSeconds;

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  void startTimer() async {
    const oneSecond = Duration(seconds: 1);
    Timer.periodic(oneSecond, (Timer t) {
      setState(() {
        if (timer < 1) {
          print('Time ended');
          timer = 30;
          updateQuestion();
        } else {
          timer = timer - 1;
        }
        if (((timer).remainder(60)) < 10) {
          withSeconds = '0${((timer).remainder(60)).toInt()}';
        } else {
          withSeconds = '${((timer).remainder(60)).toInt()}';
        }
        showTimer = '${((timer ~/ 60)).toString()}:$withSeconds';
        // showTimer = timer.toString();
      });
    });
  }

  final _itemExtent = 50.0;
  final generatedList = List.generate(4, (index) => 'Item $index');

  void updateQuestion() {
    setState(() {
      if (questionNumber == quiz.questions.length - 1) {
        var scoreResultDisplay = finalScore;
        finalScore = 0;
        questionNumber = 0;
        timer = 30;
        totalTime = 30;
        Navigator.push(
          context,
          new MaterialPageRoute(
            builder: (context) =>
            new Summary(
              score: scoreResultDisplay,
            ),
          ),
        );
      } else {
        questionNumber++;
        timer = 30;
        totalTime = 30;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // bool pressAttention = false;
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0.5,
          backgroundColor: Colors.white,
          title: Padding(
            padding: EdgeInsets.only(right: 0.0),
            child: GestureDetector(
              onTap: () {
                print('');
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.close,
                      color: Colors.grey,
                    ),
                    onPressed: () {
                      return showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return buildAlertDialog(context);
                        },
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
        body: SafeArea(
          child: CustomScrollView(
            slivers: [
              SliverList(
                delegate: SliverChildListDelegate([
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          decoration: BoxDecoration(),
                          child: Padding(
                            padding: const EdgeInsets.all(5),
                            child: Stack(
                              children: [
                                CircleAvatar(
                                  radius: 30,
                                  backgroundColor: Color(0xffdedede),
                                  child: CircleAvatar(
                                    radius: 28,
                                    backgroundColor: Colors.redAccent,
                                    child: CircleAvatar(
                                      radius: 26,
                                      backgroundImage: AssetImage(
                                          'assets/images/female.png'),
                                    ),
                                  ),
                                ),
                                Positioned(
                                  bottom: 0,
                                  right: 0,
                                  child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(70)),
                                        gradient: LinearGradient(
                                          begin: Alignment.topLeft,
                                          end: Alignment.bottomRight,
                                          colors: <Color>[
                                            Colors.red,
                                            Colors.black
                                          ],
                                        )),
                                    child: Padding(
                                      padding: const EdgeInsets.all(3.0),
                                      child: Text(
                                        '$finalScore',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(),
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Text(
                              'VS',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Color(0xff999999)),
                            ),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(),
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Stack(
                              children: [
                                CircleAvatar(
                                  radius: 30,
                                  backgroundColor: Color(0xffdedede),
                                  child: CircleAvatar(
                                    radius: 28,
                                    backgroundColor: Colors.blueAccent,
                                    child: CircleAvatar(
                                      radius: 26,
                                      backgroundImage: AssetImage(
                                          'assets/images/profile.jpg'),
                                    ),
                                  ),
                                ),
                                Positioned(
                                  bottom: 0,
                                  right: 0,
                                  child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(70)),
                                        gradient: LinearGradient(
                                          begin: Alignment.topLeft,
                                          end: Alignment.bottomRight,
                                          colors: <Color>[
                                            Colors.blue,
                                            Colors.black
                                          ],
                                        )),
                                    child: Padding(
                                      padding: const EdgeInsets.all(3.0),
                                      child: Text(
                                        '43',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                            'QUESTION NUMBER',
                            style: TextStyle(
                              color: Color(0xff999999),
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: Color(0xff00618a),
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                          ),
                          child: Text(
                            '${questionNumber + 1}/${quiz.questions.length}',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                ]),
              ),
              SliverAppBar(
                automaticallyImplyLeading: false,
                elevation: 1,
                backgroundColor: Colors.white,
                expandedHeight: 5,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                  title: Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          child: Text(
                            'TIME',
                            style: TextStyle(
                              color: Color(0xff999999),
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding:
                            const EdgeInsets.only(left: 8.0, right: 8.0),
                            child: Container(
                              child: LinearProgressIndicator(
                                minHeight: 5,
                                value: ((totalTime - timer) / totalTime),
                                backgroundColor: Colors.black38,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Color(0xfffdfdfd)),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: Color(0xff717171),
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                          ),
                          child: showTimer == null
                              ? Text(
                            '00',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          )
                              : Text(
                            showTimer,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                  centerTitle: true,
                ),
              ),
              SliverList(
                delegate: SliverChildListDelegate([
                  SizedBox(
                    height: 40,
                  ),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        quiz.questions[questionNumber],
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                            color: Color(0xff00618a)),
                      ),
                    ),
                  ),
                ]),
              ),
              SliverFixedExtentList(
                itemExtent: _itemExtent, // I'm forcing item heights
                delegate: SliverChildBuilderDelegate(
                      (context, index) =>
                      ListTile(
                        title: ButtonTheme(
                          minWidth: double.infinity,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: RaisedButton(
                            elevation: 1,
                            color: Color(0xffefefef),
                            // color: pressAttention ? Colors.red : Colors.blue,
                            onPressed: () {
                              print('${quiz.questions.length} ${index + 1}');
                              if (quiz.choices[questionNumber][index] ==
                                  quiz.correctAnswers[questionNumber]) {
                                debugPrint("Correct");
                                finalScore++;
                              } else {
                                debugPrint("Wrong");
                              }
                              updateQuestion();
                            },
                            child: Text(quiz.choices[questionNumber][index]),
                          ),
                        ),
                      ),
                  childCount: generatedList.length,
                ),
              ),
              SliverFillRemaining(
                hasScrollBody: false,
                // Switch for different overscroll behavior in your layout.
                // If your ScrollPhysics do not allow for overscroll, setting
                // fillOverscroll to true will have no effect.
                fillOverscroll: true,
                child: Container(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          FloatingActionButton.extended(
                            heroTag: 'SKIP',
                            backgroundColor: Color(0xff00618a),
                            onPressed: null,
                            label: Text('SKIP'),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          FloatingActionButton.extended(
                            backgroundColor: Color(0xff00618a),
                            heroTag: 'NEXT',
                            onPressed: () {
                              setState(() {
                                ++questionNumber;
                                if (questionNumber >
                                    quiz.questions.length - 1) {
                                  questionNumber = 0;
                                }
                              });
                            },
                            label: Text('NEXT'),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  AlertDialog buildAlertDialog(BuildContext context) {
    return AlertDialog(
      title: Text(
        'Exit Game',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 18),
      ),
      // content: Text('Challenges list here',
      //     textAlign: TextAlign.center),
      // actionsOverflowButtonSpacing: 90,
      content: Container(
          child: Text('By exiting the game, . . . . . . Continue to exit?'),
      ),
      actions: [
        buildActionOnDialog(context),
      ],
    );
  }

  Padding buildActionOnDialog(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
      child: Container(
        width: MediaQuery
            .of(context)
            .size
            .width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 100,
              child: ElevatedButton(
                style: cancelButton,
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'NO',
                ),
              ),
            ),
            Container(
              width: 100,
              child: ElevatedButton(
                style: exitButton,
                onPressed: () {
                finalScore = 0;
                questionNumber = 0;
                Navigator.pushReplacementNamed(
                  context,
                  '/home',
                );
              },
                child: Text('EXIT'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
