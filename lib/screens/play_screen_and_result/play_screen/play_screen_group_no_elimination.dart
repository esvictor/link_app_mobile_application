import 'package:flutter/material.dart';
import 'package:link_app/screens/home/components/homeWidgets/homeWidget.dart';
import 'package:link_app/routes.dart';
import './timeSlider/timeSlider.dart';
import 'package:link_app/screens/play_screen_and_result/play_screen/question_and_answer_samples.dart';
import 'package:link_app/screens/play_screen_and_result/result_summary/result_summary.dart';

class GroupWithoutElimination extends StatefulWidget {
  GroupWithoutElimination({Key key}) : super(key: key);
  static String routeName = "/GroupWithoutElimination";

  @override
  _GroupWithoutEliminationState createState() =>
      _GroupWithoutEliminationState();
}

var finalScore = 0;
var questionNumber = 0;
var quiz = new QuestionAnswers();

class _GroupWithoutEliminationState extends State<GroupWithoutElimination> {
  final List<String> entries = <String>['A', 'B', 'C', 'D', 'E', 'F'];

  final _itemExtent = 50.0;
  final generatedList = List.generate(4, (index) => 'Item $index');

  void updateQuestion() {
    setState(() {
      if (questionNumber == quiz.questions.length - 1) {
        var scoreResultDisplay = finalScore;
        finalScore = 0;
        questionNumber = 0;
        Navigator.push(
          context,
          new MaterialPageRoute(
            builder: (context) => new Summary(
              score: scoreResultDisplay,
            ),
          ),
        );
      } else {
        questionNumber++;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0.5,
          backgroundColor: Colors.white,
          title: Padding(
            padding: EdgeInsets.only(right: 0.0),
            child: GestureDetector(
              onTap: () {
                print('');
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.close,
                      color: Colors.grey,
                    ),
                    onPressed: () {
                      return showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text(
                              'Exit Game',
                              textAlign: TextAlign.center,
                            ),
                            content: Text('Are you sure you want to exit?',
                                textAlign: TextAlign.center),
                            actionsOverflowButtonSpacing: 90,
                            actions: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: Text('CANCEL'),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      finalScore = 0;
                                      questionNumber = 0;
                                      Navigator.pushReplacementNamed(
                                        context,
                                        '/home',
                                      );
                                    },
                                    child: Text(
                                      'EXIT',
                                    ),
                                  ),
                                ],
                              )
                            ],
                          );
                        },
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
        body: SafeArea(
          child: CustomScrollView(
            slivers: [
              SliverList(
                delegate: SliverChildListDelegate([
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: SizedBox(
                      height: 70,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            decoration: BoxDecoration(),
                            child: Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Stack(
                                children: [
                                  CircleAvatar(
                                    radius: 30,
                                    backgroundColor: Color(0xffdedede),
                                    child: CircleAvatar(
                                      radius: 28,
                                      backgroundColor: Colors.redAccent,
                                      child: CircleAvatar(
                                        radius: 26,
                                        backgroundImage: AssetImage(
                                            'assets/images/female.png'),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    bottom: 0,
                                    right: 0,
                                    child: Container(
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(70)),
                                          gradient: LinearGradient(
                                            begin: Alignment.topLeft,
                                            end: Alignment.bottomRight,
                                            colors: <Color>[
                                              Colors.red,
                                              Colors.black
                                            ],
                                          )),
                                      child: Padding(
                                        padding: const EdgeInsets.all(3.0),
                                        child: Text(
                                          '$finalScore',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(),
                            child: Padding(
                              padding: const EdgeInsets.only(left: 5, right: 5),
                              child: Text(
                                'VS',
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Color(0xff999999)),
                              ),
                            ),
                          ),
                          Expanded(
                            child: SingleChildScrollView(
                              child: Column(
                                children: [
                                  Container(
                                    color: Color(0xffffffff),
                                    height: 70,
                                    child: ListView.builder(
                                        physics: BouncingScrollPhysics(),
                                        scrollDirection: Axis.horizontal,
                                        padding: const EdgeInsets.only(
                                            top: 5, bottom: 5),
                                        itemCount: entries.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Material(
                                            color: Color(0xffffffff),
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  right: 5.0),
                                              child: Stack(
                                                children: [
                                                  CircleAvatar(
                                                    radius: 30,
                                                    backgroundColor:
                                                        Color(0xffdedede),
                                                    child: CircleAvatar(
                                                      radius: 28,
                                                      backgroundColor:
                                                          Colors.lightBlue,
                                                      child: CircleAvatar(
                                                        radius: 26,
                                                        backgroundImage: AssetImage(
                                                            'assets/images/profile.jpg'),
                                                      ),
                                                    ),
                                                  ),
                                                  Positioned(
                                                    bottom: 0,
                                                    right: 0,
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                          Radius.circular(70),
                                                        ),
                                                        gradient:
                                                            LinearGradient(
                                                          begin:
                                                              Alignment.topLeft,
                                                          end: Alignment
                                                              .bottomRight,
                                                          colors: <Color>[
                                                            Colors.blue,
                                                            Colors.black
                                                          ],
                                                        ),
                                                      ),
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(3.0),
                                                        child: Text(
                                                          '0',
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          );
                                        }),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ]),
              ),
              SliverList(
                delegate: SliverChildListDelegate([
                  Container(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                            'QUESTION NUMBER',
                            style: TextStyle(
                              color: Color(0xff999999),
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: Color(0xff00618a),
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                          ),
                          child: Text(
                            '${questionNumber + 1}/${quiz.questions.length}',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                ]),
              ),
              SliverAppBar(
                automaticallyImplyLeading: false,
                elevation: 1,
                backgroundColor: Colors.white,
                expandedHeight: 5,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                  title: TimeSlider(),
                  centerTitle: true,
                ),
              ),
              SliverList(
                delegate: SliverChildListDelegate([
                  SizedBox(
                    height: 40,
                  ),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        quiz.questions[questionNumber],
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                            color: Color(0xff00618a)),
                      ),
                    ),
                  ),
                ]),
              ),
              SliverFixedExtentList(
                itemExtent: _itemExtent, // I'm forcing item heights
                delegate: SliverChildBuilderDelegate(
                  (context, index) => ListTile(
                    title: ButtonTheme(
                      minWidth: double.infinity,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: RaisedButton(
                        elevation: 1,
                        color: Color(0xffefefef),
                        onPressed: () {
                          print('${quiz.questions.length} ${index + 1}');
                          if (quiz.choices[questionNumber][index] ==
                              quiz.correctAnswers[questionNumber]) {
                            debugPrint("Correct");
                            finalScore++;
                          } else {
                            debugPrint("Wrong");
                          }
                          updateQuestion();
                        },
                        child: Text(quiz.choices[questionNumber][index]),
                      ),
                    ),
                  ),
                  childCount: generatedList.length,
                ),
              ),
              SliverFillRemaining(
                hasScrollBody: false,
                // Switch for different overscroll behavior in your layout.
                // If your ScrollPhysics do not allow for overscroll, setting
                // fillOverscroll to true will have no effect.
                fillOverscroll: true,
                child: Container(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          FloatingActionButton.extended(
                            heroTag: 'SKIP',
                            backgroundColor: Color(0xff00618a),
                            onPressed: null,
                            label: Text('SKIP'),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          FloatingActionButton.extended(
                            backgroundColor: Color(0xff00618a),
                            heroTag: 'NEXT',
                            onPressed: () {
                              setState(() {
                                ++questionNumber;
                                if (questionNumber >
                                    quiz.questions.length - 1) {
                                  questionNumber = 0;
                                }
                              });
                            },
                            label: Text('NEXT'),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
