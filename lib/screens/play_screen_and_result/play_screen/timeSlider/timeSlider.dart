import 'package:flutter/material.dart';
import 'dart:async';

class TimeSlider extends StatefulWidget {
  TimeSlider({Key key}) : super(key: key);

  @override
  _TimeSliderState createState() => _TimeSliderState();
}

class _TimeSliderState extends State<TimeSlider> {
  @override
  void initState() {
    starttimer();
    super.initState();
  }

  double timer = 7000;
  String showTimer;
  double totalTime = 7000;
  String withSeconds;

  void starttimer() async {
    const onesec = Duration(milliseconds: 10);
    Timer.periodic(onesec, (Timer t) {
      setState(() {
        if (timer < 1) {
          print('Time ended');
          timer = 7000;
        } else {
          timer = timer - 1;
        }
        if (((timer ~/ 100).remainder(60)) < 10) {
          withSeconds = '0${((timer ~/ 100).remainder(60)).toString()}';
        } else {
          withSeconds = '${((timer ~/ 100).remainder(60)).toString()}';
        }
        showTimer = '${((timer ~/ 100) ~/ 60).toString()}:$withSeconds';
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            child: Text(
              'TIME',
              style: TextStyle(
                color: Color(0xff999999),
                fontSize: 12,
                fontWeight: FontWeight.normal,
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: Container(
                child: LinearProgressIndicator(
                  minHeight: 5,
                  value: ((totalTime - timer) / totalTime),
                  backgroundColor: Colors.black38,
                  valueColor: AlwaysStoppedAnimation<Color>(Color(0xfffdfdfd)),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: Color(0xff717171),
              borderRadius: BorderRadius.all(Radius.circular(5)),
            ),
            child: Text(
              showTimer,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}
